package Game.EjemploScore;

import java.awt.Font;

import Core.Field;
import Core.Sprite;
import Game.Parte4.UIs.UIComponent;

public class text extends Sprite{

	public text(int x1, int y1, int size, String path, Field f){
		super("texto", x1, y1, x1, y1, 0, path);
		this.text = true;
		this.solid = false;
		this.font = new Font("Monospaced", Font.BOLD, size);
		this.textColor = 0xFFFFFF;
	}

}
