package Game.Parte4;

import Core.Field;
import Core.Window;
import Game.Parte3.Inputs;
import Game.Parte4.Enemics.EnemicConArmadura;
import Game.Parte4.Enemics.Spawner;
import Game.Parte4.UIs.UI;

/**
 * 3 Buffos
 */
public class Main{

	public static Field f = new Field();
	public static Window w = new Window(f);
	public static boolean inGame = true;

	private static Nau nau;

	public static UI ui;

	public static void main(String[] args) throws InterruptedException{
		init();
		while (inGame){
			input();
			f.draw();
			Thread.sleep(30);
		}
		w.close();
		System.exit(0);
	}

	private static void input(){
		if((w.getPressedKeys().contains('w') || w.getPressedKeys().contains(' ')) && nau.canShoot)
			nau.move(Inputs.DISPARO);
		if(w.getKeysDown().contains('e'))
			nau.fogeo();
		if(w.getPressedKeys().contains('a') && nau.x1 > 0)
			nau.move(Inputs.IZQUIERDA);
		else if(w.getPressedKeys().contains('d') && nau.x2 < f.getWidth())
			nau.move(Inputs.DERERCHA);
		else
			nau.move(Inputs.QUIETO);
	}

	private static void init(){
		w.setSize((int) (w.getWidth() * 0.8), (int) (w.getWidth() * 1.1));
		f.setSize(w.getSize());
		w.setResizable(false);
		nau = new Nau(w.getWidth() / 2, w.getHeight() - w.getHeight() / 7, "res/b84.png", f);
		f.background = "res/darkBlue.png";

		new Spawner(f);
		ui = UI.getInstance(20, 50, 20, f.getHeight() - 75, f);
	}

}
