package Game.Parte4.UIs;

import java.util.ArrayDeque;

import Core.Field;
import Core.Sprite;
import Game.Parte4.Nau;

public class UI implements UIComponent{

	private static UI instance = null;
	public Score score = null;

	public ArrayDeque<HP> vidas = new ArrayDeque<HP>();
	public ArrayDeque<Fogeo> fogeos = new ArrayDeque<Fogeo>();

	private UI(int sx, int sy, int vx, int vy, Field f){
		this.score = new Score(sx, sy, f);
		for (int i = 1; i <= Nau.vida; i++){
			vidas.push(new HP((int) ((vx + (i * Nau.width * 1.5)) - 50), vy, f));
		}
		for (int i = 1; i <= Nau.fogeos; i++){
			fogeos.add(new Fogeo((int) (f.getWidth() - ((vx + (i * Nau.width * 1.5)) - 10)), vy-1, f));
		}
	}

	public static UI getInstance(int sx, int sy, int vx, int vy, Field f){
		if(instance == null){
			instance = new UI(sx, sy, vx, vy, f);
		}
		return instance;
	}

	public static UI getInstance(){
		return instance;
	}

}
