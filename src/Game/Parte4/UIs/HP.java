package Game.Parte4.UIs;

import Core.Field;
import Core.Sprite;
import Game.Parte4.Nau;

public class HP extends Sprite implements UIComponent{

	public HP(int x1, int y1, Field f){
		super("HP", x1, y1, x1 + Nau.width, (int) (y1 + (Nau.height) / 1.5f), 0, "res/b84.png", f);
		this.solid = false;
	}

}
