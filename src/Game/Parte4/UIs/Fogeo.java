package Game.Parte4.UIs;

import Core.Field;
import Core.Sprite;
import Game.Parte4.Nau;

public class Fogeo extends Sprite implements UIComponent{

	public Fogeo(int x1, int y1, Field f){
		super("Fogeo", x1, y1, x1 + Nau.width, (int) (y1 + (Nau.height) / 1.5f), 0, "res/mandarina.png", f);
		this.solid = false;
	}

}
