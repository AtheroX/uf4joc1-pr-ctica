package Game.Parte4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Game.Parte3.Inputs;
import Game.Parte4.Enemics.Enemic;
import Game.Parte4.UIs.UI;

public class Nau extends PhysicBody{

	private final int MOVESPEED = 9;
	private final int SHOOTCOOLDOWN = 375;

	public static boolean runningBuff = false;
	public static boolean speedShootingBuff = false;
	public static boolean multipleShootingBuff = false;

	public static int width = 32;
	public static int height = 50;
	public static int vida = 3;
	public static int fogeos = 1;

	public boolean canShoot = true, alreadyScheduled = false;

	private int movespd = 9;
	private int shootcd = 400;

	public Nau(int x1, int y1, String path, Field f){
		super("nave", x1 - (width / 2), y1 - (height / 2), x1 + (width / 2), y1 + (width / 2), 0, path, f);
		this.trigger = true;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update(){
		if(runningBuff)
			movespd = (int) (MOVESPEED * 1.7f);
		else
			movespd = MOVESPEED;

		if(speedShootingBuff)
			shootcd = (int) (SHOOTCOOLDOWN / 1.6f);
		else
			shootcd = SHOOTCOOLDOWN;

		if(!canShoot && !alreadyScheduled){
			alreadyScheduled = true;
			timer.schedule(new TimerTaskShoot(), shootcd);
		}
	}

	public void move(Inputs in){
		// {{ Input movement
		if(in == Inputs.DERERCHA){
			setVelocity(movespd, velocity[1]);
		} else if(in == Inputs.IZQUIERDA){
			setVelocity(-movespd, velocity[1]);
		} else if(in == Inputs.QUIETO){
			setVelocity(0, velocity[1]);
		} else if(in == Inputs.DISPARO){
			this.canShoot = false;
			if(multipleShootingBuff){
				new Proyectil(this, "res/mandarina.png", f, true);
				new Proyectil(this, "res/mandarina.png", f, false);
			}
			new Proyectil(this, "res/mandarina.png", f);
		}
		// }}
	}

	public static void hit(){
		System.out.println("hit");
		Nau.vida--;
		UI.getInstance().vidas.pop().delete();
		if(Nau.vida == 0){
			Main.inGame = false;
		}
	}

	public void fogeo(){
		if(fogeos > 0){
			for (Sprite s : f.sprites){
				if(s instanceof Enemic){
					s.delete();
				} else if(s instanceof Proyectil){
					Proyectil p = (Proyectil) s;
					if(p.imAmBad)
						s.delete();
				}
			}
			fogeos--;
			UI.getInstance().fogeos.removeLast().delete();
		}

	}

	static Timer timer = new Timer();

	private class TimerTaskShoot extends TimerTask{
		@Override
		public void run(){
			canShoot = true;
			alreadyScheduled = false;
		}
	};

	// {{ colls

	@Override
	public void onCollisionEnter(Sprite sprite){
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite){
		// TODO Auto-generated method stub

	}
	// }}

}
