package Game.Parte4.Enemics;

import java.util.Random;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Game.Parte4.Buff;
import Game.Parte4.BuffType;
import Game.Parte4.Nau;
import Game.Parte4.Proyectil;
import Game.Parte4.UIs.UI;

public class EnemicConArmadura extends PhysicBody implements Enemic{

	private int hp = 2;
	private int velY;

	public EnemicConArmadura(int x1, int y1, int velY, Field f){
		super("enemy", x1 - (width / 2), y1 - (height / 2), x1 + (width / 2), y1 + (width / 2), 0, "res/red.png", f);
		this.trigger = true;
		this.velY = velY;
		setVelocity(0, velY);
	}

	@Override
	public void update(){
		if(this.y1 > this.f.getHeight())
			this.delete();
	}

	// {{ colls

	@Override
	public void onTriggerEnter(Sprite sprite){
		if(sprite instanceof Proyectil){
			Proyectil p = (Proyectil) sprite;
			if(!p.imAmBad){
				hp--;
				setVelocity(0, velY + 5);
				sprite.delete();

				if(hp == 0){
					this.delete();
					UI.getInstance().score.addScore(200);

					Random r = new Random();
					if(r.nextInt(6) == 0)
						new Buff(BuffType.values()[r.nextInt(BuffType.values().length)], this.x1, this.y2, f);
				}
			}
		}
		if(sprite instanceof Nau){
			Nau.hit();
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite){
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite){
		// TODO Auto-generated method stub

	}
}
