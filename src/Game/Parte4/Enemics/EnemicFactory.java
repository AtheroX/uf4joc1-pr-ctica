package Game.Parte4.Enemics;

import java.util.Random;

import Core.Field;

public class EnemicFactory{

	Random r = new Random();
	
	public Enemic generateEnemic(int x1, int y1, int velY, Field f){
		int rand = r.nextInt(5);
		switch (rand){
		case 0:
		case 1:
		case 2:
			return new EnemicBasic(x1, y1, velY, f, (int) rand/2);
		case 3:
			return new EnemicDisparador(x1, y1, velY, f);
		case 4:
			return new EnemicConArmadura(x1, y1, velY, f);

		default:
			return null;
		}
	}
}
