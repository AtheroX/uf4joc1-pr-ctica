package Game.Parte4.Enemics;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Game.Parte4.Buff;
import Game.Parte4.BuffType;
import Game.Parte4.Nau;
import Game.Parte4.Proyectil;
import Game.Parte4.UIs.UI;

public class EnemicBasic extends PhysicBody implements Enemic{

	private boolean goingRight = true;
	private int velX = 4, zigzagChangeCD = 1500;

	public EnemicBasic(int x1, int y1, int velY, Field f, int i){
		super("enemy", x1 - (width / 2), y1 - (height / 2), x1 + (width / 2), y1 + (width / 2), 0, "res/b85.png", f);
		this.trigger = true;

		setVelocity(0, velY);
		if(i == 1){
			goingRight = new Random().nextBoolean();
			int xRepos = new Random().nextInt(f.getWidth() - 400) + 200;
			this.x1 = xRepos - (width / 2);
			this.x2 = xRepos + (width / 2);
			timer.schedule(taskZigZag, 0, zigzagChangeCD);
		}
	}

	@Override
	public void update(){
		if(this.y1 > this.f.getHeight())
			this.delete();

	}

	Timer timer = new Timer();
	TimerTask taskZigZag = new TimerTask(){

		@Override
		public void run(){
			if(goingRight)
				setVelocity(velX, velocity[1]);
			else
				setVelocity(-velX, velocity[1]);

			goingRight = !goingRight;
		}
	};

	// {{ colls

	@Override
	public void onTriggerEnter(Sprite sprite){
		if(sprite instanceof Proyectil){
			Proyectil p = (Proyectil) sprite;
			if(!p.imAmBad){
				sprite.delete();
				this.delete();
				UI.getInstance().score.addScore(100);

				Random r = new Random();
				if(r.nextInt(10) == 0)
					new Buff(BuffType.values()[r.nextInt(BuffType.values().length)], this.x1, this.y2, f);			
			}
		}
		if(sprite instanceof Nau){
			Nau.hit();
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite){
		// TODO Auto-generated method stub
	}

	@Override
	public void onCollisionExit(Sprite sprite){
		// TODO Auto-generated method stub
	}
	// }}
}
