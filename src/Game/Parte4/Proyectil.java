package Game.Parte4;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Proyectil extends PhysicBody{

	private static int size = 30;
	private static int velY = -24;
	public boolean imAmBad = false;

	public Proyectil(Sprite parent, String path, Field f){
		super("", parent.x1 + (parent.x2 - parent.x1) / 2 - size / 2, parent.y1,
				parent.x1 + (parent.x2 - parent.x1) / 2 + size / 2, parent.y1 + size, 0, path, f);
		this.trigger = true;
		this.setVelocity(0, velY);
	}

	public Proyectil(Sprite parent, String path, Field f, boolean haciaDerecha){
		super("", parent.x1 + (parent.x2 - parent.x1) / 2 - size / 2, parent.y1,
				parent.x1 + (parent.x2 - parent.x1) / 2 + size / 2, parent.y1 + size, 0, path, f);
		this.trigger = true;
		if(haciaDerecha)
			this.setVelocity(3, velY);
		else
			this.setVelocity(-3, velY);
	}

	public Proyectil(Sprite parent, String path, Field f, boolean enemigo, int speed){
		super("", parent.x1 + (parent.x2 - parent.x1) / 2 - size / 2, parent.y1,
				parent.x1 + (parent.x2 - parent.x1) / 2 + size / 2, parent.y1 + size, 0, path, f);
		this.trigger = true;
		this.imAmBad = enemigo;
		if(enemigo) {
			this.setVelocity(0, speed-(velY/3));
		}else {
			this.setVelocity(0, speed+velY);
		}
	}

	// {{ Coll
	@Override
	public void onTriggerEnter(Sprite sprite){
		if(sprite instanceof Nau && imAmBad){
			Nau.hit();
			this.delete();
		}
	}

	@Override
	public void onCollisionEnter(Sprite sprite){
	}

	@Override
	public void onCollisionExit(Sprite sprite){

	}
	// }}

	@Override
	public void update(){
		if(this.y2 < 0 || this.y1 > f.getHeight())
			this.delete();
	}

}
