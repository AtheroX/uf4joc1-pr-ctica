package Game.Parte1;

import Core.Sprite;

public class Roca extends Sprite {

	public int accionesDisponibles = 50;
	public int id = 0;
	public static int count = 0;
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		id = Roca.count;
		Roca.count++;
		System.out.println("Soy la roca con id "+ id + ". Hay "+Roca.count+" rocas en total");
	}
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, int accionesDisponibles) {
		super(name, x1, y1, x2, y2, angle, path);
		this.accionesDisponibles = accionesDisponibles;
		id = Roca.count;
		Roca.count++;
		System.out.println("Soy la roca con id "+ id + ". Hay "+Roca.count+" rocas en total");
	}
	
	public Roca(int x1, int y1, int x2, int y2, int accionesDisponibles) {
		super("Roca", x1, y1, x2, y2, 0, "res/b84.png");
		this.accionesDisponibles = accionesDisponibles;
		id = Roca.count;
		Roca.count++;
		System.out.println("Soy la roca con id "+ id + ". Hay "+Roca.count+" rocas en total");
	}
	
	public Roca(int x1, int y1, int size) {
		super("Roca", x1, y1, x1+size, y1+size, 0, "res/b84.png");
		this.accionesDisponibles = 50;
		id = Roca.count;
		Roca.count++;
		System.out.println("Soy la roca con id "+ id + ". Hay "+Roca.count+" rocas en total");
	}
	
	public Roca() {
		super("Roca", 0, 0, 50, 50, 0, "res/b84.png");
		this.accionesDisponibles = 50;
		id = Roca.count;
		Roca.count++;
		System.out.println("Soy la roca con id "+ id + ". Hay "+Roca.count+" rocas en total");
	}
	
	

}
