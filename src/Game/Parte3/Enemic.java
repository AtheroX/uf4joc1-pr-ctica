package Game.Parte3;

import Core.PhysicBody;
import Core.Sprite;

public class Enemic extends PhysicBody implements Disparable {

	int vida;
	static int enemyCounter = 0;
	static int width = 32;
	static int height = 50;
	
	public Enemic(int x1, int y1, int vida) {
		super("Enemy"+enemyCounter, x1, y1, x1+width, y1+height, 0, "res/red.png");
		this.vida = vida;
		enemyCounter++;
		setConstantForce(0, Main.GRAVITYFORCE);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void danyar() {
		this.vida--;
		if(this.vida <= 0) {
			this.delete();
		}
	}

}
