package Game.Parte3;

import java.awt.Color;

import Core.PhysicBody;
import Core.Sprite;
import Game.Parte2.Plataforma;

public class Proyectil extends PhysicBody {
	
	public static int moveSpeed = 15;
	public static int maxBotes = 4;
	private Sprite parent;
	private int actualBotes;
	
	int size = 30;

	public Proyectil(String name, Sprite parent, String path) {
		super(name, 0,0,0,0,0, path);
		this.parent = parent;
		reposition(parent);
		trigger = true;
		actualBotes = maxBotes;
		setVelocity(Proyectil.moveSpeed, 0);
		setConstantForce(0, Main.GRAVITYFORCE);
	}
	
	public Proyectil(Proyectil p, boolean right) {
		super(p.name, p.x1, p.y1, p.x2, p.y2, p.angle, p.path);
		reposition(p.parent);
		trigger = true;
		actualBotes = maxBotes;
		if(right)
			setVelocity(Proyectil.moveSpeed, -10);
		else
			setVelocity(-Proyectil.moveSpeed, -10);
			
		setConstantForce(0, Main.GRAVITYFORCE);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
//		System.out.println("coll with "+sprite.name); //imprimeix objecte si ha colisionat

		if(sprite instanceof Plataforma && velocity[1] >= 0) {
			if(actualBotes<1)
				delete();
			
			double vel = (velocity[1]*0.5d);
			System.out.println("in"+vel);
			vel = vel < 8d ? vel+(8d-vel) : vel;
			System.out.println(vel);
			setVelocity(velocity[0]*0.85f, 0);
			addForce(0, -vel);
			this.actualBotes--;
		}else if(sprite instanceof Disparable) { //es pot fer un instanceof a l'interf�cie, no nom�s la classe
            Disparable d = (Disparable) sprite; //casteig a disparable
            d.danyar(); //cridem a danyar
            System.out.println("pum");
            this.delete(); //esborrem el projectil
        }
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

	}

	public void reposition(Sprite p) {
		this.x1 = p.x1+((p.x2-p.x1)/2)-(size/2);
		this.x2 = p.x1+((p.x2-p.x1)/2)+(size/2);
		this.y1 = p.y1+((p.y2-p.y1)/2)-(size/2);
		this.y2 = p.y1+((p.y2-p.y1)/2)+(size/2);
	}

}
