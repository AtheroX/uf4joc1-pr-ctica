package Game.Parte3;

import Core.PhysicBody;
import Core.Sprite;
import Game.Parte2.Plataforma;

public class Personaje extends PhysicBody{

	private final int JUMPFORCE = 12;
	private final int MOVESPEED = 9;

	private static Proyectil proyectil;
	private static int width = 32;
	private static int height = 50;

	public boolean lookingRight = true;

	private boolean canShoot = true;
	private boolean onGround = false, canJump = false;

	private int minPantalla, maxPantalla;
	private int jumpCount = 0, maxJumps = 2;
	private int jumpCD = 25, jumpTime = 0;
	private int shootCD = 5, shootTime = 0;
	private int[] spawnpoint = new int[2];

	public Personaje(String name, int x1, int y1, String path){
		super(name, x1, y1, x1 + width, y1 + height, 0, path);
		spawnpoint[0] = x1;
		spawnpoint[1] = y1;
		setConstantForce(0, Main.GRAVITYFORCE);

		minPantalla = (Main.w.getWidth() / 10) * 4;
		maxPantalla = (Main.w.getWidth() / 10) * 6;

		proyectil = new Proyectil("Proy", this, "res/mandarina.png");
	}

	public void moviment(Inputs in){

		// {{ SCROLL
		if(this.x2 < minPantalla){
			Main.f.scroll(MOVESPEED, 0);
			minPantalla -= MOVESPEED;
			maxPantalla -= MOVESPEED;
		} else if(this.x1 > maxPantalla){
			Main.f.scroll(-MOVESPEED, 0);
			minPantalla += MOVESPEED;
			maxPantalla += MOVESPEED;
		}
		// }}

		// {{ JUMPS
		if(!onGround){
			jumpTime++;
		}
		if(!onGround && jumpCount < maxJumps && jumpTime >= jumpCD){
			canJump = true;
			changeImage("res/b84.png");
		}
		// }}

		// {{ Shooting
		if(!canShoot){
			if(shootTime >= shootCD){
				canShoot = true;
				shootTime = 0;
			} else{
				shootTime++;
			}
		}
		// }}

		// {{ Input movement
		if(in == Inputs.SALTO){
			if(onGround || canJump){
				setVelocity(velocity[0], 0);
				addForce(0, -JUMPFORCE);
				jumpCount++;
				canJump = false;
				onGround = false;
				changeImage("res/b85.png");
			}
		}
		if(in == Inputs.DERERCHA){
			setVelocity(MOVESPEED, velocity[1]);
		} else if(in == Inputs.IZQUIERDA){
			setVelocity(-MOVESPEED, velocity[1]);
		} else if(in == Inputs.QUIETO){
			setVelocity(0, velocity[1]);
		}
		// }}

	}

	@Override
	public void onCollisionEnter(Sprite sprite){
		if((sprite instanceof Plataforma || sprite instanceof Obstacle) && this.y2 <= sprite.y1){
			onGround = true;
			canJump = true;
			jumpCount = 0;
			jumpTime = 0;
			changeImage("res/b84.png");
		} else if(sprite instanceof Enemic){
			this.x1 = spawnpoint[0];
			this.x2 = spawnpoint[0] + width;
			this.y1 = spawnpoint[1];
			this.y2 = spawnpoint[1] + height;
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite){

	}

	public void shoot(){
		if(canShoot){
			Main.sprites.add(new Proyectil(proyectil, lookingRight));
			canShoot = false;
		}
	}

}
