package Game.Parte2;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;
import Game.Parte1.Roca;
import Game.Parte2.Personaje;

public class Main2 {
	
	private static Field f = new Field();
	private static Window w = new Window(f);
	
	private static Personaje pj = new Personaje("", 50, 50, "res/b84.png");
	private static RocaControlable rc = new RocaControlable("", 100, 100, 150, 150, 0, "res/b84.png");
	private static Plataforma floor = new Plataforma(0, f.getHeight()-10, f.getWidth(), 10, "res/black.png");
	
	public static void main(String[] args) throws InterruptedException {
		boolean inGame = true;
		
		while(inGame) {
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
//			sprites.add(rc);
//			input(rc);
			sprites.add(floor);
			sprites.add(pj);
			input(pj);
			f.draw(sprites);
			Thread.sleep(30);
		}
	}
	
	private static void input(RocaControlable rc) {
		if(w.getPressedKeys().contains('w')) {
			rc.moviment(Inputs.AMUNT);
		}
		if(w.getPressedKeys().contains('a')) {
			rc.moviment(Inputs.ESQUERRA);
		}else if(w.getPressedKeys().contains('d')) {
			rc.moviment(Inputs.DRETA);
		}
		if(w.getPressedKeys().contains('s')) {
			rc.moviment(Inputs.AVALL);
		}
	}

	private static void input(Personaje pj) {
		if(w.getPressedKeys().contains('w')) {
			pj.moviment(Inputs.AMUNT);
		}
		if(w.getPressedKeys().contains('a')) {
			pj.moviment(Inputs.ESQUERRA);
		}else if(w.getPressedKeys().contains('d')) {
			pj.moviment(Inputs.DRETA);
		}else {
			pj.moviment(null);
		}
	}

}
