package Game.Parte2;

import Core.PhysicBody;
import Core.Sprite;

public class Plataforma extends PhysicBody {

	public Plataforma(int x1, int y1, int width, int height, String path) {
		super("Plataforma", x1, y1, x1+width, y1+height, 0, path);
		this.solid = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub

	}

}
