package Game.Parte2;

import Core.Sprite;
import Game.Parte1.Roca;;

public class RocaControlable extends Roca {

	private int moveSpeed = 2;
	
	public RocaControlable(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		// TODO Auto-generated constructor stub
	}

	public void moviment(Inputs in) {
		System.out.println(this.accionesDisponibles);
		if(this.accionesDisponibles <= 0 && !this.delete) {
			this.delete();
		}else {
			this.accionesDisponibles--;
		}
		
		if(!this.delete) {
			if(in == Inputs.AMUNT) {
				this.y1 -= this.moveSpeed;
				this.y2 -= this.moveSpeed;
				
			}else if(in == Inputs.AVALL) {
				this.y1 += this.moveSpeed;
				this.y2 += this.moveSpeed;
				
			}else if(in == Inputs.DRETA) {
				this.x1 += this.moveSpeed;
				this.x2 += this.moveSpeed;
				
			}else if(in == Inputs.ESQUERRA) {
				this.x1 -= this.moveSpeed;
				this.x2 -= this.moveSpeed;
				
			}
		}
	}
}
