package Game.Parte2;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Main {
	
	private static Field f = new Field();
	private static Window w = new Window(f);
	
	public static RocaControlable roca = new RocaControlable("Roca", 50, 50, 100, 100, 0, "res/b84.png");
	
	public static void main(String[] args) throws InterruptedException {
		boolean inGame = true;
		
		while(inGame) {
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(roca);
			input();
			f.draw(sprites);
			Thread.sleep(30);
		}
	}
	
	private static void input() {
		if(w.getPressedKeys().contains('w')) {
			roca.moviment(Inputs.AMUNT);
			
		}
		if(w.getPressedKeys().contains('a')) {
			roca.moviment(Inputs.ESQUERRA);
			
		}
		if(w.getPressedKeys().contains('s')) {
			roca.moviment(Inputs.AVALL);
			
		}
		if(w.getPressedKeys().contains('d')) {
			roca.moviment(Inputs.DRETA);
			
		}
	}

}
