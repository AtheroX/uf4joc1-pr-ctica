package Game.Parte2;

import Core.PhysicBody;
import Core.Sprite;
import Game.Parte2.Inputs;
import Game.Parte2.Plataforma;

public class Personaje extends PhysicBody {

	private final int JUMPFORCE = 12;
	
	private final int MOVESPEED = 9;
	static int width = 32;
	static int height = 50;
		
	private boolean onGround = false, canJump = false;
	private int jumpCount = 0, maxJumps = 2;
	
	int jumpCD = 20, jumpTime = 0;

	public Personaje(String name, int x1, int y1, String path) {
		super(name, x1, y1, x1+width, y1+height, 0, path);
		setConstantForce(0, Game.Parte3.Main.GRAVITYFORCE);
	}

	public void moviment(Inputs in) {
		if(!onGround) {
			jumpTime++;
		}
		if(!onGround && jumpCount<maxJumps && jumpTime>=jumpCD) {
			canJump = true;
			changeImage("res/b84.png");			
		}
		
		if(in == Inputs.AMUNT) {
			if(onGround || canJump) {
				setVelocity(velocity[0], 0);
				addForce(0, -JUMPFORCE);
				jumpCount++;
				canJump = false;
				onGround = false;
				changeImage("res/b85.png");
			}
		}
		if(in == Inputs.DRETA) {
			setVelocity(MOVESPEED, velocity[1]);
		}else if(in == Inputs.ESQUERRA) {
			setVelocity(-MOVESPEED, velocity[1]);			
		}else if(in == null){
			setVelocity(0, velocity[1]);
		}
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Plataforma) {
			onGround = true;
			jumpCount = 0;
			jumpTime = 0;
			changeImage("res/b84.png");
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

	}

}
